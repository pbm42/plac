;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; 4 Op-blocks world
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(define (domain SINGE)
  (:requirements :strips)
  (:predicates 	(estbas ?x)
				(esthaut ?x)
				(holding ?x)
				(handempty)
				(esta ?s ?p)
				(estSinge ?x)
				(estBanane ?x)
				(estCaisse ?x)
	       )

  (:action aller
	     :parameters (?s ?sp ?ep)
	     :precondition (and (estSinge ?s) (estbas ?s) (esta ?s ?sp))
	     :effect
	     (and	(not (esta ?s ?sp))
				(esta ?s ?ep)))

  (:action pousser
	     :parameters (?s ?c ?sp ?ep)
	     :precondition (and (estSinge ?s) (estCaisse ?c) (estbas ?s)(esta ?s ?sp) (esta ?c ?sp))
	     :effect
	     (and	(esta ?s ?ep)
				(esta ?c ?ep)
				(not(esta ?s ?sp))
				(not(esta ?c ?sp))))
				
  (:action monter
	     :parameters (?s ?p ?c)
	     :precondition (and (estSinge ?s) (estCaisse ?c) (esta ?s ?p) (esta ?c ?p) (estbas ?s))
	     :effect
	     (and	(not (estbas ?s))
				(esthaut ?s)))
		 
  (:action descendre
	     :parameters (?s)
	     :precondition (and (esthaut ?s) (estSinge ?s))
	     :effect
	     (and	(not(esthaut ?s))
				(estbas ?s)))

  (:action attraper
	     :parameters (?s ?p ?b)
	     :precondition (and (estSinge ?s) (estBanane ?b) (esthaut ?s) (esthaut ?b) (handempty) (esta ?s ?p) (esta ?b ?p))
	     :effect
	     (and	(not(handempty))
				(holding ?b)))
				
  (:action lacher
	     :parameters (?s ?b ?p)
	     :precondition (and (estbas ?s) (estSinge ?s) (holding ?b) (estBanane ?b) (esta ?s ?p))
	     :effect
	     (and	(handempty)
				(not(holding ?b))
				(estbas ?b)
				(esta ?b ?p)))
)